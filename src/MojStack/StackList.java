package MojStack;

import java.util.LinkedList;

public class StackList extends MojStack {

    private LinkedList<Object> items = new LinkedList<>();

    @Override
    public void push(Object e) {
        items.addFirst(e);
    }

    @Override
    public boolean isEmpty() {
        return items.size() == 0;
    }

    @Override
    public Object pop() {
        return items.removeFirst();
    }
}
