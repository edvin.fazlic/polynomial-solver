package MojStack;

import java.rmi.server.ExportException;
import java.util.Collections;

public class StackArray extends MojStack{

    private Object[] items;
    private int size = 0;

    public StackArray(int capacity){
        items = new Object[capacity];
    }

    @Override
    public void push(Object e) {
        if (size == items.length){
            Object[] newItems = new Object[2*items.length];
            System.arraycopy(items, 0, newItems, 0, items.length);
            items = newItems;
        }
        items[size++] = e;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Object pop() {
        return items[size-- -1];
    }
}
