package MojStack;

abstract class MojStack {
    public abstract void push(Object e);
    public abstract boolean isEmpty();
    public abstract Object pop();
}

