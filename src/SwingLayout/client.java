package SwingLayout;

import javax.swing.*;
import java.awt.*;

public class client {
    public static void main(String[] args) {
        new CustomFrame();
    }
}

class CustomFrame extends JFrame{
    CustomFrame(){
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setSize(1200, 700);
        super.setLocation(350, 150);

        super.add(new CustomPanel());

        super.setVisible(true);
    }
}

class CustomPanel extends JPanel{
    private JButton button1 = new JButton("Button 1");
    private JButton button2 = new JButton("Button 2");
    private JButton button3 = new JButton("Button 3");
    private JButton button4 = new JButton("Button 4");
    private JButton button5 = new JButton("Button 5");
    private JButton button6 = new JButton("Button 6");

    CustomPanel(){
        super.setLayout(new BorderLayout());
        JPanel northPanel = new JPanel();
        northPanel.add(button1);
        northPanel.add(button2);
        northPanel.setBackground(Color.red);

        JPanel southPanel = new JPanel();
        southPanel.add(button3);

        southPanel.setBackground(Color.gray);

        JPanel westPanel = new JPanel();
        westPanel.add(button5);
        westPanel.setBackground(Color.green);

        JPanel eastPanel = new JPanel();
        eastPanel.add(button6);
        eastPanel.setBackground(Color.blue);

        JPanel centerPanel = new JPanel();
        centerPanel.add(button4);
        centerPanel.setBackground(Color.yellow);
        centerPanel.setLayout(new GridLayout(3, 3));

        super.add(northPanel, BorderLayout.NORTH);
        super.add(eastPanel, BorderLayout.EAST);
        super.add(westPanel, BorderLayout.WEST);
        super.add(southPanel, BorderLayout.SOUTH);
        super.add(centerPanel, BorderLayout.CENTER);
    }
}