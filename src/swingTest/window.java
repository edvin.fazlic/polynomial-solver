package swingTest;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class window extends JFrame{

    int counter = 0;
    JButton button1 = new JButton("Increment");
    JLabel text1 = new JLabel("0");

    public window(){
        frameSettings();

        button1.addActionListener(new Listener());

        text1.setSize(100, 50);
        button1.setBounds(400, 300, 100, 70);
        text1.setBounds(350, 300, 100, 70);
        this.add(button1);
        this.add(text1);

        this.setVisible(true);
    }

    private void frameSettings(){
        this.setSize(1200, 670);
        this.setLocation(360, 200);
        this.setLayout(null);
        this.setVisible(true);
        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private class Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == button1){
                text1.setText(String.valueOf(++counter));
                print(String.format("Set counter to %d", counter));
            }
        }
    }

    public static void print(Object o){
        System.out.println(o);
    }
}
