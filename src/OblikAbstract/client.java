package OblikAbstract;

public class client {
    public static void main(String[] args) {
        Oblik[] obl = new Oblik[2];
        obl[0] = new Krug(3);
        obl[1] = new Pravougaonik(2,3);
        for (Oblik oblik : obl) System.out.println(oblik.povrsina());

        Krug d = new Krug(2.2213123);

        myInterface pomnozi = (a, b) -> a*b;
        myInterface saberi = (a, b) -> a+b;

        Oblik a = new Oblik() {
            double a = 3;
            @Override
            double povrsina() {
                return a;
            }
        };

        System.out.println(saberi.saberi(5.666, -7.42));

    }
}

