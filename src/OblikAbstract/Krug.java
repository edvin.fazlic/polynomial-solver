package OblikAbstract;

public class Krug extends Oblik {
    private double radius;
    public Krug(double r){
        radius = r;
    }
    @Override
    double povrsina() {
        return radius*radius*Math.PI;
    }

}

