package OblikAbstract;

public class Pravougaonik extends Oblik {

    private double a,b;
    public Pravougaonik(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    double povrsina() {
        return a*b;
    }
}
