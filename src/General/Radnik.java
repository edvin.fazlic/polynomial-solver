package General;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Radnik extends Zaposlenik{

    public Radnik(String ime, double plata, GregorianCalendar datumZaposljenja){
        this.ime = ime;
        this.plata = plata;
        this.datumZaposljenja = datumZaposljenja;
    }

    @Override
    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String d = df.format(datumZaposljenja.getTime());
        return String.format("%s %.2f %s",ime,plata,d);
    }

}
