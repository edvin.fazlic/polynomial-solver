package General;


public class Vlasnik extends Zaposlenik{
    public Vlasnik(String ime, double plata){
        this.ime = ime;
        this.plata = plata;
    }

    @Override
    public String toString() {
        return String.format("%s %.2f",ime,plata);
    }
}
