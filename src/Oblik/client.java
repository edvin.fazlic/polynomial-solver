package Oblik;

public class client {
    public static void main(String[] args) {
        int[] nizBrojeva = new int[10];

        for(int i = 0; i < 10; i++){
            nizBrojeva[i] = i+1;
        }

        Kvadrat a = new Kvadrat();
        Ispis s = new Ispis();

        for(int i = 0; i < 10; i++){
            s.izracunaj(a.izracunaj(s.izracunaj(nizBrojeva[i])));
        }
    }

}

class Ispis implements Funkcija{
    @Override
    public int izracunaj(int a) {
        System.out.println(a);
        return a;
    }
}
