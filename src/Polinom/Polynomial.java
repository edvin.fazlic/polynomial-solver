package Polinom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

public class Polynomial implements Cloneable {

    private ArrayList<Double> coefficients = new ArrayList<>();

    public Polynomial(double...inputItems){
        for(double i : inputItems){
            this.coefficients.add(i);
        }
        Collections.reverse(coefficients);
    }

    public Polynomial(ArrayList<Double> inputList){
        this.coefficients.addAll(inputList);
        Collections.reverse(this.coefficients);
    }

    public void add(Polynomial inputPolynomial){
        int thisSize = coefficients.size();
        int otherSize = inputPolynomial.coefficients.size();
        int sizeDiff = otherSize - thisSize;

        if (sizeDiff < 0){
            for(int i = 0; i < otherSize; i++){
                coefficients.set(i, coefficients.get(i)+inputPolynomial.coefficients.get(i));
            }
        } else {
            for(int i = 0; i < sizeDiff; i++){
                coefficients.add(0.0);
            }

            for(int i = 0; i < coefficients.size(); i++){
                coefficients.set(i, coefficients.get(i)+inputPolynomial.coefficients.get(i));
            }
        }
    }

    public void mult(Polynomial inputPolynomial){
        ArrayList<Double> newCoefficients = new ArrayList<>();
        int thisSize = coefficients.size();
        int otherSize = inputPolynomial.coefficients.size();
        for(int i = 0; i < thisSize+otherSize-1; i++)
            newCoefficients.add(0.0);

        for (int i = 0; i < thisSize; i++){
            for (int j = 0; j < otherSize; j++){
                newCoefficients.set(i+j, (newCoefficients.get(i+j) + coefficients.get(i) * inputPolynomial.coefficients.get(j)));
            }
        }

        coefficients = newCoefficients;
    }

    public static ComplexNumber find2ComplexZero(Polynomial inputPolynomial){
        System.out.println(String.format("Finding complex zeros for f(x) = %s", inputPolynomial));
        if (inputPolynomial.coefficients.size() != 3)
            return new ComplexNumber();

        double a = inputPolynomial.coefficients.get(2);
        double b = inputPolynomial.coefficients.get(1);
        double c = inputPolynomial.coefficients.get(0);

        double val = Math.pow(b, 2) - 4*a*c;
        int counter = 1;

        //complex result
        double a1 = -b/(2*a);
        double a2 = Math.sqrt(-val)/(2*a);
        ComplexNumber c1 = new ComplexNumber(a1, a2);
        System.out.println(String.format("x%d = %s, f(x%d) = %s", counter, c1, counter++, evaluate(inputPolynomial, c1)));
        ComplexNumber c2 = new ComplexNumber(a1, -a2);
        System.out.println(String.format("x%d = %s, f(x%d) = %s", counter, c2, counter, evaluate(inputPolynomial, c2)));

        return c1;
    }

    private static Polynomial div(Polynomial inputPolynomial, Polynomial b){
        ArrayList<Double> items = new ArrayList<>();

        for(int i = 0; i < inputPolynomial.coefficients.size(); i++)
            items.add(0.0);

        int currentWorkingPosition = inputPolynomial.coefficients.size()-1;
        final int dividerPosition = b.coefficients.size()-1;

        while(currentWorkingPosition >= dividerPosition){
            if (b.coefficients.get(dividerPosition) == 0)
                System.out.println("DIJELJENJE SA 0!!!");

            double divResult = inputPolynomial.coefficients.get(currentWorkingPosition) / b.coefficients.get(dividerPosition);
            items.set(currentWorkingPosition - dividerPosition, divResult);

            //subtract
            for(int i = 0; i < b.coefficients.size(); i++){
                double trenutniKoef = b.coefficients.get(i);
                inputPolynomial.coefficients.set(currentWorkingPosition - dividerPosition + i, inputPolynomial.coefficients.get(currentWorkingPosition - dividerPosition + i) - trenutniKoef*divResult);
            }

            currentWorkingPosition--;
        }

        for(int i=items.size()-1; i > inputPolynomial.coefficients.size()-b.coefficients.size(); i--){
            items.remove(i);
        }
        Collections.reverse(items);

        return new Polynomial(items);
    }

    private static double evaluate(Polynomial inputPolynomial, double inputValue){
        double result = 0;

        int exp = 0;
        for (double coef : inputPolynomial.coefficients){
            result += coef*Math.pow(inputValue, exp++);
        }

        return result;
    }
    private static ComplexNumber evaluate(Polynomial inputPolynomial, ComplexNumber inputValue){
        ComplexNumber result = new ComplexNumber();

        int exp = 0;

        for (double coef : inputPolynomial.coefficients){
            ComplexNumber pow = inputValue;
            for (int i = 0; i < exp; i++){
                pow = ComplexNumber.mult(pow, inputValue);
            }
            result = ComplexNumber.add(result, ComplexNumber.mult(pow, coef));
            exp++;
        }

        return result;
    }

    //Find a zero with the Newton Raphson Method
    private static double newtonRhapsonReal(Polynomial inputPolynomial) {
        //derivative of input Polynomialial
        final Polynomial derivativeInputPolynomial = inputPolynomial.derivative();
        //precision required to break the loop
        final double differenceThreshold = 1E-6;
        //number of iterations after which the loop gives up
        final int maxIterations = (int) 1E6;
        int iterCounter = 0;

        double Xn = 0.5;
        double Xn1 = 0.5;
        boolean loopCondition = true;

        //Newton Raphson Method
        while(loopCondition){
            //divisor cant be 0
            if (evaluate(derivativeInputPolynomial, Xn) == 0){
                Xn += 0.05;
            }
            //Xn+1 = Xn - f(Xn)/f'(Xn)
            Xn1 = Xn - (evaluate(inputPolynomial, Xn)/evaluate(derivativeInputPolynomial, Xn));
            //If Xn+1 ~ Xn we have reached an adequate accuracy, or if we pass a certain number of interactions we give up
            if (Math.abs(Xn1-Xn) < differenceThreshold)
                loopCondition = false;
            if (iterCounter++ > maxIterations){
                //System.out.println(String.format("Loop limit reached, giving up at Xn+1: %.6f", Xn1));
                loopCondition = false;
            }
            Xn = Xn1;
        }

        if (iterCounter >= maxIterations)
            return Double.NaN;
        else
            return Xn1;
    }

    private static ComplexNumber newtonRhapsonComplex(Polynomial inputPolynomial) {
        //derivative of input Polynomialial
        final Polynomial derivativeInputPolynomial = inputPolynomial.derivative();
        //precision required to break the loop
        final double differenceThreshold = 1E-6;
        //number of iterations after which the loop gives up
        final int maxIterations = (int) 1E6;
        int iterCounter = 0;

        ComplexNumber Xn = new ComplexNumber();
        ComplexNumber Xn1 = new ComplexNumber();
        boolean loopCondition = true;

        //Newton Raphson Method
        while(loopCondition){
            //divisor cant be 0
            if (evaluate(derivativeInputPolynomial, Xn).real() == 0 && evaluate(derivativeInputPolynomial, Xn).imaginary() == 0){
                Xn.real(0.1);
                Xn.imaginary(0.1);
            }
            //Xn+1 = Xn - f(Xn)/f'(Xn)
            Xn1 = ComplexNumber.sub(Xn, ComplexNumber.div(evaluate(inputPolynomial, Xn), evaluate(derivativeInputPolynomial, Xn)));
            //If Xn+1 ~ Xn we have reached an adequate accuracy, or if we pass a certain number of interactions we give up
            if (Math.abs(Xn1.real()-Xn.real()) < differenceThreshold && Math.abs(Xn1.imaginary()-Xn.imaginary()) < differenceThreshold)
                loopCondition = false;
            if (iterCounter++ > maxIterations){
                //System.out.println(String.format("Loop limit reached, giving up at Xn+1: %.6f", Xn1));
                loopCondition = false;
            }
            Xn = Xn1;
        }

        if (Math.abs(Xn1.real()) < differenceThreshold)
            Xn1.real(-Xn1.real());

        if (Math.abs(Xn1.imaginary()) < differenceThreshold)
            Xn1.imaginary(-Xn1.imaginary());

        if (iterCounter >= maxIterations)
            return new ComplexNumber(Double.NaN, Double.NaN);
        else
            return Xn1;
    }

    public static void findZeros(Polynomial input) {
        Polynomial inputPolynomial = new Polynomial();
        inputPolynomial.coefficients = (ArrayList<Double>) input.coefficients.clone();

        for(int i = 0; i < 60; i++){
            System.out.print("-");
        }
        System.out.println();

        int order = inputPolynomial.coefficients.size()-1;
        System.out.println(String.format("f(x) = %s\n\nPolinom %d. reda\n", inputPolynomial, order));

        //find real zeros
        boolean realExists = true;
        Polynomial temp = inputPolynomial;

        ArrayList<Double> realZeros = new ArrayList<>();

        while(realExists){
            double result = newtonRhapsonReal(temp);
            if (Double.isNaN(result)){
                realExists = false;
            }else{
                realZeros.add(result);
                temp = Polynomial.div(temp, new Polynomial(1, -result));
            }
        }

        //find complex zeros
        ArrayList<ComplexNumber> complexZeros = new ArrayList<>();

        for(int i = 1; i < order-realZeros.size(); i+=2){
            ComplexNumber result = newtonRhapsonComplex(temp);
            ComplexNumber result2 = ComplexNumber.conj(result);

            complexZeros.add(result);
            complexZeros.add(result2);

            Polynomial divi = new Polynomial(1,
                    ComplexNumber.add(ComplexNumber.mult(result, -1), ComplexNumber.mult(result2, -1)).real(),
                    ComplexNumber.mult(result, result2).real());

            temp = Polynomial.div(temp, divi);
        }

        complexZeros.stream().filter(it -> Math.abs(it.imaginary()) <= 1E-4).collect(Collectors.toCollection(ArrayList::new)).forEach(it -> realZeros.add(it.real()));
        complexZeros = complexZeros.stream().filter(it -> Math.abs(it.imaginary()) > 1E-4).collect(Collectors.toCollection(ArrayList::new));

        Collections.sort(realZeros);
        Collections.sort(complexZeros);

        System.out.println(String.format("Realne nule: %d\n", realZeros.size()));

        for (double zero : realZeros){
            System.out.println(String.format("x = %f -> f(x) = %f", zero, evaluate(input, zero)));
        }

        System.out.println(String.format("\nKompleksne nule: %d\n", complexZeros.size()));

        for (ComplexNumber zero : complexZeros){
            System.out.println(String.format("x = %s -> f(x) = %s", zero, evaluate(input, zero)));
        }
    }

    public Polynomial derivative(){
        Polynomial ret = new Polynomial();

        for(int i = 0; i < coefficients.size()-1; i++){
            ret.coefficients.add(0.0);
            ret.coefficients.set(i, coefficients.get(i+1)*(i+1));
        }

        return ret;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        final char unknown = 'x';
        boolean isPositive = true;
        int exp = -1;

        for (double coef : coefficients){
            String thisNumber = "";
            exp++;

            if (coef == 0) {
                continue;
            } else if (coef < 0) {
                thisNumber += " - ";
            } else {
                thisNumber += " + ";
            }

            isPositive = coef > 0;

            if(!(Math.abs(coef) == 1 && exp!=0))
                thisNumber += Math.abs(coef);

            if (exp == 0) {
                ret.insert(0, thisNumber);
                continue;
            } else if (exp == 1) {
                thisNumber += unknown;
            } else {
                thisNumber += unknown + "^" + exp;
            }

            ret.insert(0, thisNumber);
        }

        try {
            if (isPositive) {
                return ret.substring(3);
            } else {
                return ret.substring(1);
            }
        }catch (Exception e){
            return "(empty Polynomialial)";
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Polynomial ret = (Polynomial) super.clone();
        ret.coefficients = (ArrayList<Double>) coefficients.clone();
        return ret;
    }
}

