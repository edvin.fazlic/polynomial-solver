package Polinom;

import org.jetbrains.annotations.NotNull;

import java.lang.invoke.StringConcatFactory;

public class ComplexNumber implements Comparable{
    private double a,b;

    public ComplexNumber(double a, double b){
        this.a = a;
        this.b = b;
    }

    public ComplexNumber(){
        this.a = 0;
        this.b = 0;
    }

    public static ComplexNumber add(ComplexNumber c1, ComplexNumber c2){
        return new ComplexNumber(c1.a+c2.a, c1.b+c2.b);
    }

    public static ComplexNumber sub(ComplexNumber c1, ComplexNumber c2){
        return new ComplexNumber(c1.a-c2.a, c1.b-c2.b);
    }

    public static ComplexNumber conj(ComplexNumber c1){
        return new ComplexNumber(c1.a, -c1.b);
    }

    public static ComplexNumber mult(ComplexNumber c1, double d){
        return new ComplexNumber(c1.a*d, c1.b*d);
    }

    public static ComplexNumber div(ComplexNumber c1, ComplexNumber c2){
        double a = (c1.a*c2.a+c1.b*c2.b)/(Math.pow(c2.a, 2)+Math.pow(c2.b, 2));
        double b = (c1.b*c2.a-c1.a*c2.b)/(Math.pow(c2.a, 2)+Math.pow(c2.b, 2));
        return new ComplexNumber(a, b);
    }

    public double real(){
        return a;
    }

    public double imaginary(){
        return b;
    }

    public void real(double s){
        a += s;
    }

    public void imaginary(double s){
        b += s;
    }

    public static ComplexNumber mult(ComplexNumber c1, ComplexNumber c2){
        return new ComplexNumber((c1.a*c2.a) - (c1.b*c2.b), (c1.a*c2.b) + (c1.b*c2.a));
    }

    @Override
    public String toString() {
        final char complexSimbol = 'i';
        String ret = "";

        if (a < 0 && Math.abs(a) > 1E-6){
            ret += "- ";
        }

        ret += String.format("%.6f", Math.abs(a));

        if (b < 0 && Math.abs(b) > 1E-6){
            ret += " - ";
        }else{
            ret += " + ";
        }

        ret += String.format("%.6f", Math.abs(b));

        ret += complexSimbol;
        return ret;
    }

    @Override
    public int compareTo(@NotNull Object o) {
        ComplexNumber other = (ComplexNumber) o;

        if (Math.abs(other.a)+Math.abs(other.b) < Math.abs(a)+Math.abs(b))
            return 1;
        else if (Math.abs(other.a)+Math.abs(other.b) > Math.abs(a)+Math.abs(b))

            return -1;
        else
            return 0;
    }
}
