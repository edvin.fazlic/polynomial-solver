package Polinom;

import java.util.ArrayList;

public class client {
    public static void main(String[] args) {

        Polynomial a1 = new Polynomial(-1, -6, -1, 34, -28, -14, 7, 1);
        Polynomial a2 = new Polynomial(1, -6, -1, 34, -28, -14, 7, 1);
        Polynomial a3 = new Polynomial(2, 4, 2, 4);
        Polynomial a4 = new Polynomial(3, 2, 6);
        Polynomial a5 = new Polynomial(2, 6);
        Polynomial a6 = new Polynomial(1,0,2,0,1);
        Polynomial a7 = new Polynomial(1,0,2);
        Polynomial a8 = new Polynomial(1,-4,5);
        Polynomial a9 = new Polynomial(1,3,3,1);

        print("");

        Polynomial.findZeros(a1);
        Polynomial.findZeros(a2);
        Polynomial.findZeros(a3);
        Polynomial.findZeros(a4);
        Polynomial.findZeros(a5);
        Polynomial.findZeros(a6);
        Polynomial.findZeros(a9);
        Polynomial.findZeros(a7);
        Polynomial.findZeros(a8);


        for(int i = 0; i < 60; i++){
            System.out.print("-");
        }
        System.out.println();
    }

    public static void print(Object a){
        System.out.println(a);
    }
}
