package TestEur;

public class Kompanija {
    private String naziv;
    private Integer maticniBroj;
    private String adresa;
    private Osoba direktor;
    private Osoba predsjednik;

    public Kompanija(String naziv, Integer maticniBroj, String adresa, Osoba direktor, Osoba predsjednik){
        this.naziv = naziv;
        this.maticniBroj = maticniBroj;
        this.adresa = adresa;
        this.direktor = direktor;
        this.predsjednik = predsjednik;
    }

    public void ispisi(){
        System.out.println("Ime kompanije: "+naziv+", maticni broj: "+maticniBroj+", adresa: "+adresa);
        System.out.print("Direktor: ");
        direktor.ispisi();
        System.out.print("Predsjednik: ");
        predsjednik.ispisi();
    }
}


