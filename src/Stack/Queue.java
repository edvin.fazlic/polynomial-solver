package Stack;

import java.util.LinkedList;

public class Queue<E> {

    private LinkedList<E> items = new LinkedList<>();

    public void push(E data){
        items.add(data);
    }

    public E pop(){
        return items.removeFirst();
    }

    public E end(){
        return items.get(0);
    }

    public boolean isEmpty(){
        return items.size() == 0;
    }

}
