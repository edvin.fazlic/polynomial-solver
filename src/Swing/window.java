package Swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class window extends JFrame {

    public window(){
        this.setSize(1200, 700);
        this.setLocation(350, 150);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(new MousePanel());
        this.setTitle("Prozor");
    }

}

class MousePanel extends JPanel{
    static final int SIDE = 20;
    int location = 10;
    ArrayList<Rectangle2D> rectangle2DList = new ArrayList<>();
    Rectangle2D current = null;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2D = (Graphics2D) g;

        rectangle2DList.forEach(g2D::draw);
    }

    MousePanel(){
        super.addMouseListener(new MouseHandler());
        super.addMouseMotionListener(new MouseMotionHandler());
    }

    private void addRectangle(MouseEvent e){
        Rectangle2D r2d = new Rectangle2D.Double(e.getX()-10, e.getY()-10, SIDE, SIDE);
        location += 10;
        rectangle2DList.add(r2d);
        repaint();
    }

    private Rectangle2D findRec(MouseEvent e){
        return rectangle2DList
                .stream()
                .filter(r2d -> r2d.contains(e.getPoint()))
                .findFirst()
                .orElse(null);
    }

    private void removeRec(Rectangle2D o){
        this.rectangle2DList.remove(o);
        repaint();
    }

    public class MouseHandler implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent e) {
            if (findRec(e) == null)
                addRectangle(e);
            else
                removeRec(findRec(e));
        }

        @Override
        public void mousePressed(MouseEvent e) {
            current = findRec(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    private class MouseMotionHandler implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent e) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (current != null) {
                current.setFrame(e.getX() - 10, e.getY() - 10, SIDE, SIDE);
                repaint();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            if (findRec(e) == null){
                setCursor(Cursor.getDefaultCursor());
            }else{
                setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            }
        }
    }
}
